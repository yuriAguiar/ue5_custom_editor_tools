// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE5CustomEditorToolsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE5CUSTOMEDITORTOOLS_API AUE5CustomEditorToolsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
