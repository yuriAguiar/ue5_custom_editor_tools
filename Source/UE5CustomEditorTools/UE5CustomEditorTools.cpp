// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE5CustomEditorTools.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE5CustomEditorTools, "UE5CustomEditorTools" );
