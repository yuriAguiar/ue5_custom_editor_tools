// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Styling/CoreStyle.h"
#include "Styling/SlateTypes.h"
#include "Widgets/SCompoundWidget.h"
#include "Widgets/Views/SListView.h"

class SButton;
class SCheckBox;
class STextBlock;
class ITableRow;
class STableViewBase;

class SAdvancedDeletionTab : public SCompoundWidget
{
	SLATE_BEGIN_ARGS(SAdvancedDeletionTab) {}
	SLATE_ARGUMENT(TArray<TSharedPtr<FAssetData>>, AssetsDataToStore);
	SLATE_ARGUMENT(FString, CurrentlySelectedFolder);
	SLATE_END_ARGS()

public:
	void Construct(const FArguments& InArgs);

private:
#pragma region ComboBoxForListingCondition
	TSharedRef<SComboBox<TSharedPtr<FString>>> ConstructComboBox();
	TSharedRef<SWidget> OnGenerateComboContent(TSharedPtr<FString> SourceItem);
	void OnComboSelectionChanged(TSharedPtr<FString> SelectedOption, ESelectInfo::Type InSelectInfo);
	TSharedRef<STextBlock> ConstructComboHelpText(const FString& TextContent, ETextJustify::Type TextJustification);

	TArray<TSharedPtr<FString>> ComboBoxSourceItems;
	TSharedPtr<STextBlock> ComboDisplayTextBlock;
#pragma endregion
	
#pragma region RowWidgetForListView
	TSharedRef<SListView<TSharedPtr<FAssetData>>> ConstructListView();
	void RefreshListView();
	TSharedRef<ITableRow> OnGenerateRowForList(TSharedPtr<FAssetData> AssetDataToDisplay, const TSharedRef<STableViewBase>& OwnerTable);
	void OnRowMouseButtonClicked(TSharedPtr<FAssetData> ClickedAssetData);
	TSharedRef<SCheckBox> ConstructCheckBox(const TSharedPtr<FAssetData>& AssetDataToDisplay);
	TSharedRef<STextBlock> ConstructTextBox(const FString& TextContent, const FSlateFontInfo& FontInfo);
	void OnCheckBoxStateChanged(ECheckBoxState NewState, TSharedPtr<FAssetData> AssetData);
	TSharedRef<SButton> ConstructButton(const TSharedPtr<FAssetData>& AssetDataToDisplay);
	FReply OnDeleteButtonClicked(TSharedPtr<FAssetData> ClickedAssetData);
#pragma endregion

#pragma region BottomButtons
	TSharedRef<SButton> ConstructDeleteAllButton();
	TSharedRef<SButton> ConstructSelectAllButton();
	TSharedRef<SButton> ConstructDeselectAllButton();

	TSharedRef<STextBlock> ConstructButtonsText(const FString& TextContent);

	FReply OnDeleteAllButtonClicked();
	FReply OnSelectAllButtonClicked();
	FReply OnDeselectAllButtonClicked();
#pragma endregion
	
	FSlateFontInfo GetEmbossedTextFont() const
	{
		return FCoreStyle::Get().GetFontStyle(FName("EmbossedText"));
	}

	TSharedPtr<SListView<TSharedPtr<FAssetData>>> ConstructedListView;
	TArray<TSharedPtr<FAssetData>> StoredAssetsData;
	TArray<TSharedPtr<FAssetData>> DisplayedAssetsData;
	TArray<TSharedPtr<FAssetData>> AssetsDataToDeleteArray;
	TArray<TSharedRef<SCheckBox>> CheckBoxesArray;
};
