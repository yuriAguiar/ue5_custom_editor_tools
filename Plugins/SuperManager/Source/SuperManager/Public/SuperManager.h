// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "Slate/Public/Widgets/Docking/SDockTab.h"

class FExtender;

class FSuperManagerModule : public IModuleInterface
{
#pragma region ContentBrowserMenuExtention
	void InitCBMenuExtention();
	TSharedRef<FExtender> CustomCBMenuExtender(const TArray<FString>& SelectedPaths);
	void AddCBMenuEntry(class FMenuBuilder& MenuBuilder);
	void OnDeleteUnusedAssetButtonClicked();
	void OnAdvancedDeletionButtonClicked();
	void DeleteEmptyFolders();
	void FixUpRedirectors();

	TArray<FString> FolderPathsSelected;
#pragma endregion

#pragma region CustomEditorTab
	void RegisterAdvancedDeletionTab();
	TSharedRef<SDockTab> OnSpawnAdvancedDeletionTab(const FSpawnTabArgs& Args);
	TArray<TSharedPtr<FAssetData>> GetAllAssetsDataUnderSelectedFolder();
#pragma endregion
	
public:
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

#pragma region ProcessDataForAdvancedDeletion
	bool DeleteSingleAssetInAssetList(const FAssetData& AssetDataToDelete);
	bool DeleteMultipleAssetsInAssetList(const TArray<FAssetData>& AssetDataArrayToDelete);
	void ListUnusedAssetsForList(const TArray<TSharedPtr<FAssetData>>& AssetsToFilter, TArray<TSharedPtr<FAssetData>>& OutUnusedAssetsData);
	void ListSameNameAssetsForList(const TArray<TSharedPtr<FAssetData>>& AssetsToFilter, TArray<TSharedPtr<FAssetData>>& OutSameNameAssetsData);
	void SyncContentBrowserToClickedAsset(const FString& AssetPatchToSync);
#pragma endregion
};
