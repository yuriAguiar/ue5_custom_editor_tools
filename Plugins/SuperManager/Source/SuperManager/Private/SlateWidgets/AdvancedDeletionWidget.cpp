// Fill out your copyright notice in the Description page of Project Settings.

#include "SlateWidgets/AdvancedDeletionWidget.h"

#include "AssetRegistry/AssetData.h"
#include "DebugHeader.h"
#include "Slate/Public/Widgets/Text/STextBlock.h"
#include "SlateCore/Public/Widgets/SBoxPanel.h"
#include "SuperManager.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Layout/SScrollBox.h"

#define LIST_ALL TEXT("List All Availlable Assets")
#define LIST_UNUSED TEXT("List Unused Assets")
#define LIST_SAME_NAME TEXT("List Same Same Assets")

void SAdvancedDeletionTab::Construct(const FArguments& InArgs)
{
	bCanSupportFocus = true;

	StoredAssetsData = InArgs._AssetsDataToStore;
	DisplayedAssetsData = StoredAssetsData;
	
	CheckBoxesArray.Empty();
	AssetsDataToDeleteArray.Empty();
	ComboBoxSourceItems.Empty();
	
	ComboBoxSourceItems.Add(MakeShared<FString>(LIST_ALL));
	ComboBoxSourceItems.Add(MakeShared<FString>(LIST_UNUSED));
	ComboBoxSourceItems.Add(MakeShared<FString>(LIST_SAME_NAME));
	
	FSlateFontInfo TitleTextFont = GetEmbossedTextFont();
	TitleTextFont.Size = 30;

	ChildSlot
	[
		// Main vertical box
		SNew(SVerticalBox)
		// First vertical box slot for the title text
		+SVerticalBox::Slot().AutoHeight()
		[
			SNew(STextBlock)
			.Text(FText::FromString("Advanced Deletion"))
			.Font(TitleTextFont)
			.Justification(ETextJustify::Center)
			.ColorAndOpacity(FColor::White)
		]

		// Second slot for the drop down that will specify the listing condition and help text
		+SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+SHorizontalBox::Slot().AutoWidth()
			[
				ConstructComboBox()
			]

			+SHorizontalBox::Slot().FillWidth(.6f)
			[
				ConstructComboHelpText(TEXT("Specify the listing condition in the dropdown. Left mouse click to go to where the asset is located"), ETextJustify::Center)
			]

			+SHorizontalBox::Slot().FillWidth(.6f)
			[
				ConstructComboHelpText(TEXT("Current folder:\n") + InArgs._CurrentlySelectedFolder, ETextJustify::Right)
			]
		]

		// Third slot for the assets list
		+SVerticalBox::Slot().VAlign(VAlign_Fill)
		[
			SNew(SScrollBox)
			+SScrollBox::Slot()
			[
				ConstructListView()
			]
		]

		// Fourth slot for 3 buttons
		+SVerticalBox::Slot().AutoHeight()
		[
			SNew(SHorizontalBox)
			+SHorizontalBox::Slot()
			.FillWidth(10.f)
			.Padding(5.f)
			[
				ConstructDeleteAllButton()
			]

			+SHorizontalBox::Slot()
			.FillWidth(10.f)
			.Padding(5.f)
			[
				ConstructSelectAllButton()
			]

			+SHorizontalBox::Slot()
			.FillWidth(10.f)
			.Padding(5.f)
			[
				ConstructDeselectAllButton()
			]
		]
	];
}

#pragma region ComboBoxForListingCondition
TSharedRef<SComboBox<TSharedPtr<FString>>> SAdvancedDeletionTab::ConstructComboBox()
{
	TSharedRef<SComboBox<TSharedPtr<FString>>> ConstructedComboBox = SNew(SComboBox<TSharedPtr<FString>>)
	.OptionsSource(&ComboBoxSourceItems)
	.OnGenerateWidget(this, &SAdvancedDeletionTab::OnGenerateComboContent)
	.OnSelectionChanged(this, &SAdvancedDeletionTab::OnComboSelectionChanged)
	[
		SAssignNew(ComboDisplayTextBlock, STextBlock).Text(FText::FromString(TEXT("List Assets Option")))
	];

	return ConstructedComboBox;
}

TSharedRef<SWidget> SAdvancedDeletionTab::OnGenerateComboContent(TSharedPtr<FString> SourceItem)
{
	TSharedRef<STextBlock> ConstructedComboText = SNew(STextBlock)
	.Text(FText::FromString(*SourceItem.Get()));

	return ConstructedComboText;
}

void SAdvancedDeletionTab::OnComboSelectionChanged(TSharedPtr<FString> SelectedOption, ESelectInfo::Type InSelectInfo)
{
	DebugHeader::Print(*SelectedOption.Get(), FColor::Cyan);
	ComboDisplayTextBlock->SetText(FText::FromString(*SelectedOption.Get()));

	FSuperManagerModule& SuperManagerModule = FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));

	if (*SelectedOption.Get() == LIST_ALL)
	{
		DisplayedAssetsData = StoredAssetsData;
		RefreshListView();
	}
	else if (*SelectedOption.Get() == LIST_UNUSED)
	{
		SuperManagerModule.ListUnusedAssetsForList(StoredAssetsData, DisplayedAssetsData);
		RefreshListView();
	}
	else if (*SelectedOption.Get() == LIST_SAME_NAME)
	{
		SuperManagerModule.ListSameNameAssetsForList(StoredAssetsData, DisplayedAssetsData);
		RefreshListView();
	}
}

TSharedRef<STextBlock> SAdvancedDeletionTab::ConstructComboHelpText(const FString& TextContent, ETextJustify::Type TextJustification)
{
	TSharedRef<STextBlock> ConstructedHelpText = SNew(STextBlock)
	.Text(FText::FromString(TextContent))
	.Justification(TextJustification)
	.AutoWrapText(true);

	return ConstructedHelpText;
}
#pragma endregion

void SAdvancedDeletionTab::RefreshListView()
{
	CheckBoxesArray.Empty();
	AssetsDataToDeleteArray.Empty();
	
	if (ConstructedListView.IsValid())
	{
		ConstructedListView->RebuildList();
	}
}

#pragma region RowWidgetForListView
TSharedRef<SListView<TSharedPtr<FAssetData>>> SAdvancedDeletionTab::ConstructListView()
{
	ConstructedListView = SNew(SListView<TSharedPtr<FAssetData>>)
	.ItemHeight(24.f)
	.ListItemsSource(&DisplayedAssetsData)
	.OnGenerateRow(this, &SAdvancedDeletionTab::OnGenerateRowForList)
	.OnMouseButtonClick(this, &SAdvancedDeletionTab::OnRowMouseButtonClicked);

	return ConstructedListView.ToSharedRef();
}

TSharedRef<ITableRow> SAdvancedDeletionTab::OnGenerateRowForList(TSharedPtr<FAssetData> AssetDataToDisplay, const TSharedRef<STableViewBase>& OwnerTable)
{
	if (!AssetDataToDisplay.IsValid())
	{
		return SNew(STableRow<TSharedPtr<FAssetData>>, OwnerTable);
	}
	
	const FString DisplayAssetName = AssetDataToDisplay->AssetName.ToString();
	const FString DisplayAssetClassName = AssetDataToDisplay->AssetClass.ToString();
	
	FSlateFontInfo AssetClassNameFont = GetEmbossedTextFont();
	AssetClassNameFont.Size = 10;

	FSlateFontInfo AssetNameFont = GetEmbossedTextFont();
	AssetNameFont.Size = 15;
	
	TSharedRef<STableRow<TSharedPtr<FAssetData>>> ListViewRowWidget =
	SNew(STableRow<TSharedPtr<FAssetData>>, OwnerTable).Padding(FMargin(5.f))
	[
		SNew(SHorizontalBox)

		// First slot for checkbox
		+SHorizontalBox::Slot()
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Center)
		.FillWidth(.05f)
		[
			ConstructCheckBox(AssetDataToDisplay)
		]

		// Second slot for displaying the asset class name
		+SHorizontalBox::Slot()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Fill)
		.FillWidth(.5f)
		[
			ConstructTextBox(DisplayAssetClassName, AssetClassNameFont)
		]

		// Third slot for displaying the asset name
		+SHorizontalBox::Slot()
		.HAlign(HAlign_Left)
		.VAlign(VAlign_Fill)
		[
			ConstructTextBox(DisplayAssetName, AssetNameFont)
		]

		// Fourth slot for a button
		+SHorizontalBox::Slot()
		.HAlign(HAlign_Right)
		.VAlign(VAlign_Fill)
		[
			ConstructButton(AssetDataToDisplay)
		]
	];

	return ListViewRowWidget;
}

void SAdvancedDeletionTab::OnRowMouseButtonClicked(TSharedPtr<FAssetData> ClickedAssetData)
{
	FSuperManagerModule& SuperManager = FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));
	SuperManager.SyncContentBrowserToClickedAsset(ClickedAssetData->ObjectPath.ToString());
}

TSharedRef<SCheckBox> SAdvancedDeletionTab::ConstructCheckBox(const TSharedPtr<FAssetData>& AssetDataToDisplay)
{
	TSharedRef<SCheckBox> ConstructedCheckBox = SNew(SCheckBox)
	.Type(ESlateCheckBoxType::CheckBox)
	.OnCheckStateChanged(this, &SAdvancedDeletionTab::OnCheckBoxStateChanged, AssetDataToDisplay)
	.Visibility(EVisibility::Visible);

	CheckBoxesArray.Add(ConstructedCheckBox);

	return ConstructedCheckBox;
}

TSharedRef<STextBlock> SAdvancedDeletionTab::ConstructTextBox(const FString& TextContent, const FSlateFontInfo& FontInfo)
{
	TSharedRef<STextBlock> ConstructedTextBlock = SNew(STextBlock)
	.Text(FText::FromString(TextContent))
	.Font(FontInfo)
	.ColorAndOpacity(FColor::White);

	return ConstructedTextBlock;
}

void SAdvancedDeletionTab::OnCheckBoxStateChanged(ECheckBoxState NewState, TSharedPtr<FAssetData> AssetData)
{
	switch (NewState)
	{
	case ECheckBoxState::Unchecked:
		if (AssetsDataToDeleteArray.Contains(AssetData))
		{
			AssetsDataToDeleteArray.Remove(AssetData);
		}
		break;
		
	case ECheckBoxState::Checked:
		AssetsDataToDeleteArray.AddUnique(AssetData);
		break;
		
	case ECheckBoxState::Undetermined:
		break;
		
	default:
		break;
	}
}

TSharedRef<SButton> SAdvancedDeletionTab::ConstructButton(const TSharedPtr<FAssetData>& AssetDataToDisplay)
{
	TSharedRef<SButton> ConstructedButton = SNew(SButton)
	.Text(FText::FromString(TEXT("Delete")))
	.OnClicked(this, &SAdvancedDeletionTab::OnDeleteButtonClicked, AssetDataToDisplay);

	return ConstructedButton;
}

FReply SAdvancedDeletionTab::OnDeleteButtonClicked(TSharedPtr<FAssetData> ClickedAssetData)
{
	FSuperManagerModule& SuperManager = FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));
	bool bAssetDeleted = SuperManager.DeleteSingleAssetInAssetList(*ClickedAssetData.Get());

	if (bAssetDeleted)
	{
		if (StoredAssetsData.Contains(ClickedAssetData))
		{
			StoredAssetsData.Remove(ClickedAssetData);
		}

		if (DisplayedAssetsData.Contains(ClickedAssetData))
		{
			DisplayedAssetsData.Remove(ClickedAssetData);
		}

		RefreshListView();
	}
	
	return FReply::Handled();
}
#pragma endregion

#pragma region RowWidgetForListView
TSharedRef<SButton> SAdvancedDeletionTab::ConstructDeleteAllButton()
{
	TSharedRef<SButton> DeleteAllButton = SNew(SButton)
	.ContentPadding(FMargin(5.f))
	.OnClicked(this, &SAdvancedDeletionTab::OnDeleteAllButtonClicked);

	DeleteAllButton->SetContent(ConstructButtonsText(TEXT("Delete All")));

	return DeleteAllButton;
}

TSharedRef<SButton> SAdvancedDeletionTab::ConstructSelectAllButton()
{
	TSharedRef<SButton> SelectAllButton = SNew(SButton)
	.ContentPadding(FMargin(5.f))
	.OnClicked(this, &SAdvancedDeletionTab::OnSelectAllButtonClicked);

	SelectAllButton->SetContent(ConstructButtonsText(TEXT("Select All")));

	return SelectAllButton;
}

TSharedRef<SButton> SAdvancedDeletionTab::ConstructDeselectAllButton()
{
	TSharedRef<SButton> DeselectAllButton = SNew(SButton)
	.ContentPadding(FMargin(5.f))
	.OnClicked(this, &SAdvancedDeletionTab::OnDeselectAllButtonClicked);

	DeselectAllButton->SetContent(ConstructButtonsText(TEXT("Deselect All")));

	return DeselectAllButton;
}

TSharedRef<STextBlock> SAdvancedDeletionTab::ConstructButtonsText(const FString& TextContent)
{
	FSlateFontInfo ButtonTextFont = GetEmbossedTextFont();
	ButtonTextFont.Size = 15;

	TSharedRef<STextBlock> ConstructedTextBlock = SNew(STextBlock)
	.Text(FText::FromString(TextContent))
	.Font(ButtonTextFont)
	.Justification(ETextJustify::Center);

	return ConstructedTextBlock;
}

FReply SAdvancedDeletionTab::OnDeleteAllButtonClicked()
{
	if (AssetsDataToDeleteArray.Num() == 0)
	{
		DebugHeader::ShowMsgDialog(EAppMsgType::Ok, TEXT("No asset currently selected."));
		return FReply::Handled();
	}

	TArray<FAssetData> AssetDataToDelete;

	for (const TSharedPtr<FAssetData> Data : AssetsDataToDeleteArray)
	{
		AssetDataToDelete.Add(*Data.Get());
	}

	FSuperManagerModule& SuperManagerModule = FModuleManager::LoadModuleChecked<FSuperManagerModule>(TEXT("SuperManager"));
	const bool bAssetsDeleted = SuperManagerModule.DeleteMultipleAssetsInAssetList(AssetDataToDelete);

	if (bAssetsDeleted)
	{
		for (const TSharedPtr<FAssetData>& DeletedData : AssetsDataToDeleteArray)
		{
			if (StoredAssetsData.Contains(DeletedData))
			{
				StoredAssetsData.Remove(DeletedData);
			}

			if (DisplayedAssetsData.Contains(DeletedData))
			{
				DisplayedAssetsData.Remove(DeletedData);
			}
		}

		RefreshListView();
	}

	return FReply::Handled();
}

FReply SAdvancedDeletionTab::OnSelectAllButtonClicked()
{
	if (CheckBoxesArray.Num() == 0)
	{
		return FReply::Handled();
	}

	for (const TSharedRef<SCheckBox> CheckBox : CheckBoxesArray)
	{
		if (!CheckBox->IsChecked())
		{
			CheckBox->ToggleCheckedState();
		}
	}
	
	return FReply::Handled();
}

FReply SAdvancedDeletionTab::OnDeselectAllButtonClicked()
{
	if (CheckBoxesArray.Num() == 0)
	{
		return FReply::Handled();
	}

	for (const TSharedRef<SCheckBox> CheckBox : CheckBoxesArray)
	{
		if (CheckBox->IsChecked())
		{
			CheckBox->ToggleCheckedState();
		}
	}
	
	return FReply::Handled();
}
#pragma endregion
